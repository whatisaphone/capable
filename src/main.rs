#![warn(future_incompatible, rust_2018_compatibility, rust_2018_idioms, unused)]
#![warn(clippy::pedantic)]
// #![warn(clippy::cargo)]
#![cfg_attr(feature = "strict", deny(warnings))]

use image::{imageops, GenericImage, ImageBuffer, Rgba, RgbaImage};
use std::{
    convert::TryInto,
    fs,
    path::{Path, PathBuf},
};
use structopt::StructOpt;

const TRANSPARENT: Rgba<u8> = Rgba([0, 0, 0, 0]);
const BLUE: Rgba<u8> = Rgba([0, 0, 255, 255]);
const WHITE: Rgba<u8> = Rgba([255, 255, 255, 255]);
const BLACK: Rgba<u8> = Rgba([0, 0, 0, 255]);

const ARMOR_NAMES: &[&str] = &[
    "none",
    "leather",
    "chainmail",
    "platemail",
    "heavyplate",
    "gi",
    "obsidian0",
    "obsidian1",
    "obsidian2",
    "glass",
    "heavyglass0",
    "heavyglass1",
    "heavyglass2",
    "quartz",
];

const OUTLINE_COLORS: &[Rgba<u8>] = &[
    Rgba([0, 0, 0, 255]),
    Rgba([255, 255, 255, 255]),
    Rgba([255, 255, 117, 255]),
    Rgba([220, 166, 38, 255]),
];

fn main() {
    let command = Command::from_args();
    command.run();
}

#[derive(StructOpt)]
struct Command {
    /// If set, this will also install the mod after generating it. The path to
    /// the "Crypt of the Necrodancers" directory should be passed.
    #[structopt(short, long)]
    install: Option<PathBuf>,
}

impl Command {
    fn run(self) {
        let mut frames: Vec<_> = (0..4)
            .map(|i| image::open(format!("assets/body{}.png", i)).unwrap())
            .collect();
        let mut frames: Vec<_> = frames
            .iter_mut()
            .map(|i| {
                let mut image = i.to_rgba();
                change_color(&mut image, BLUE, TRANSPARENT);
                image
            })
            .collect();

        let mut armors: Vec<_> = ARMOR_NAMES
            .iter()
            .copied()
            .map(|name| image::open(format!("assets/{}.png", name)).ok())
            .collect();
        let armors: Vec<_> = armors
            .iter_mut()
            .map(|image| Some(image.as_mut()?.sub_image(1, 1, 22, 22)))
            .collect();

        let mut player1_armor_body: RgbaImage = ImageBuffer::new(24 * 16, 24 * 14);

        #[allow(clippy::needless_range_loop)]
        for vert in 0..14 {
            for horz in 0..16 {
                let frame = &mut frames[horz % 4];

                let mut sprite = spread_by_one_pixel(frame);
                let outline_color = OUTLINE_COLORS[horz / 4];
                change_color(&mut sprite, WHITE, outline_color);
                imageops::overlay(&mut sprite, frame, 0, 0);

                let armor_offset_y = [2, 1, 0, 1][horz % 4];
                if let Some(armor) = &armors[vert] {
                    let mut armor = armor.to_image();
                    change_color(&mut armor, BLACK, outline_color);
                    imageops::overlay(&mut sprite, &armor, 1, armor_offset_y);
                }

                imageops::replace(
                    &mut player1_armor_body,
                    &sprite,
                    (horz * 24).try_into().unwrap(),
                    (vert * 24).try_into().unwrap(),
                );
            }
        }

        let output_dir = Path::new("output");
        fs::create_dir_all(output_dir).unwrap();
        player1_armor_body
            .save(output_dir.join("player1_armor_body.png"))
            .unwrap();

        if let Some(game_dir) = self.install {
            fs::copy(
                output_dir.join("player1_armor_body.png"),
                game_dir
                    .join("mods")
                    .join("capable")
                    .join("entities")
                    .join("player1_armor_body.png"),
            )
            .unwrap();
        }
    }
}

fn spread_by_one_pixel(image: &mut RgbaImage) -> RgbaImage {
    let mut dest = ImageBuffer::new(image.width(), image.height());
    let subimage = image.sub_image(1, 1, 22, 22);
    for y in 0..=2 {
        for x in 0..=2 {
            imageops::overlay(&mut dest, &subimage, x, y);
        }
    }
    dest
}

fn change_color(image: &mut RgbaImage, old: Rgba<u8>, new: Rgba<u8>) {
    for pixel in image.pixels_mut() {
        if *pixel == old {
            *pixel = new;
        }
    }
}
